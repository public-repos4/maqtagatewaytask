using Core.Application.Features.Users.Queries.GetUsers;
using Core.Application.Interfaces.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Presentation.WebApi;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Core.Application.Interfaces;
using AutoMapper;
using System.Linq;

namespace WebApiTest
{
	[TestClass]
	public class UsersServiceTest
	{
		readonly IServiceProvider _services = Program.CreateHostBuilder(new string[] { }).Build().Services; // one liner

		private IUserRepositoryAsync _userRepository;
		private IModelHelper _modelHelper;
		private IMapper _mapper;
		private IDataShapeHelper<GetUserDto> _dataShaper;

		[TestInitialize]
		public void Setup()
		{
			_userRepository = _services.GetService<IUserRepositoryAsync>();
			_modelHelper = _services.GetService<IModelHelper>();
			_mapper = _services.GetService<IMapper>();
			_dataShaper = _services.GetService<IDataShapeHelper<GetUserDto>>();
		}

		[TestMethod]
		public async Task GetUser_WhereFirstNameEqualtToWajeeh_UserWajeehIsExists()
		{
			//Arrange
			GetAllUsersQueryHandler getAllUsersQueryHandler = new(_userRepository, _modelHelper, _mapper, _dataShaper);

			//Act
			var allUsersWithFirstNameEqualtToWajeehResponse = await getAllUsersQueryHandler.Handle(new GetUsersQuery() { FirstName = "Wajeeh" }, new System.Threading.CancellationToken());

			//Assert
			Assert.IsTrue(allUsersWithFirstNameEqualtToWajeehResponse.Data.Count() > 0);
		}

		[TestMethod]
		public async Task CheckUsers_WhereEmailIsConfirmed_AllUsersEmailsConfirmed()
		{
			//Arrange
			GetAllUsersQueryHandler getAllUsersQueryHandler = new(_userRepository, _modelHelper, _mapper, _dataShaper);

			//Act
			var allUsersWithConfirmedEmailResponse = await getAllUsersQueryHandler.Handle(new GetUsersQuery() { EmailConfirmed = true, PageSize = int.MaxValue }, new System.Threading.CancellationToken());
			var allUsersResponse = await getAllUsersQueryHandler.Handle(new GetUsersQuery() { PageSize = int.MaxValue }, new System.Threading.CancellationToken());

			//Assert
			Assert.IsTrue(allUsersWithConfirmedEmailResponse.Data.Count() == allUsersResponse.Data.Count());
		}
	}
}
