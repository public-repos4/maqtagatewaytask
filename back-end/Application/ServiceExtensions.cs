﻿using Core.Application.Behaviours;
using Core.Application.Features.Users.Queries.GetUsers;
using Core.Application.Features.Roles.Queries.GetRoles;
using Core.Application.Helpers;
using Core.Application.Interfaces;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Core.Application
{
	public static class ServiceExtensions
	{
		public static void AddApplicationLayer(this IServiceCollection services)
		{
			services.AddAutoMapper(Assembly.GetExecutingAssembly());
			services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
			services.AddMediatR(Assembly.GetExecutingAssembly());
			services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));

			services.AddScoped<IDataShapeHelper<GetUserDto>, DataShapeHelper<GetUserDto>>();
			services.AddScoped<IDataShapeHelper<GetRoleDto>, DataShapeHelper<GetRoleDto>>();

			services.AddScoped<IModelHelper, ModelHelper>();
		}
	}
}