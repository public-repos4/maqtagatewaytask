﻿using Core.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Users.Commands.CreateUser
{
	public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
	{
		private readonly IUserRepositoryAsync _userRepository;
		private readonly IRoleRepositoryAsync _roleRepository;

		public CreateUserCommandValidator(
			IUserRepositoryAsync userRepository,
			IRoleRepositoryAsync roleRepository
			)
		{
			this._userRepository = userRepository;
			this._roleRepository = roleRepository;

			RuleFor(entity => entity.FirstName)
				.NotEmpty().WithMessage("'{PropertyName}' is required.")
				.NotNull();

			RuleFor(entity => entity.LastName)
				.NotEmpty().WithMessage("'{PropertyName}' is required.")
				.NotNull();

			RuleFor(p => p.UserName)
				.NotEmpty().WithMessage("{PropertyName} is required.")
				.NotNull()
				.MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
				.MustAsync(BeUniqueUserName).WithMessage("{PropertyName} already exists.");

			RuleFor(entity => entity.RoleId)
				.NotEmpty().WithMessage("'{PropertyName}' is required.")
				.NotNull()
				.MustAsync(BeExistRoleId).WithMessage("{PropertyName} not exist.");

			//RuleFor(entity => entity.Password)
			//	.NotEmpty().WithMessage("'{PropertyName}' is required.")
			//	.NotNull();
		}

		private async Task<bool> BeUniqueUserName(string userName, CancellationToken cancellationToken)
		{
			return await _userRepository.IsUniqueUserNameAsync(userName);
		}

		private async Task<bool> BeExistRoleId(long roleId, CancellationToken cancellationToken)
		{
			return await _roleRepository.IsExistedRoleIdAsync(roleId);
		}
	}
}