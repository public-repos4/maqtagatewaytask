﻿using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Core.Domain.Entities.Identity;

namespace Core.Application.Features.Users.Commands.CreateUser
{
	public partial class CreateUserCommand : IRequest<Response<long>>
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string UserName { get; set; }
		public long RoleId { get; set; }
		public string Password { get; set; }
		public string PhoneNumber { get; set; }
		public string MobileNumber { get; set; }
		public byte Age { get; set; }
		public string Address { get; set; }
		public string Email { get; set; }

	}

	public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Response<long>>
	{
		private readonly IUserRepositoryAsync _userRepository;
		private readonly IMapper _mapper;

		public CreateUserCommandHandler(IUserRepositoryAsync userRepository, IMapper mapper)
		{
			_userRepository = userRepository;
			_mapper = mapper;
		}

		public async Task<Response<long>> Handle(CreateUserCommand command, CancellationToken cancellationToken)
		{
			var user = _mapper.Map<User>(command);
			await _userRepository.AddAsync(user, command.RoleId);
			return new Response<long>(user.Id, "SucceededCreate");
		}
	}
}