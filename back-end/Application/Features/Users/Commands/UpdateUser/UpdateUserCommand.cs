﻿	using AutoMapper;
using Core.Application.Exceptions;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Users.Commands.UpdateUser
{
	public class UpdateUserCommand : IRequest<Response<long>>
	{
		public long Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string UserName { get; set; }
		public long RoleId { get; set; }
		public string Password { get; set; }
		public string PhoneNumber { get; set; }
		public string MobileNumber { get; set; }
		public byte Age { get; set; }
		public string Email { get; set; }
		public string Address { get; set; }
	}

	public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, Response<long>>
	{
		private readonly IUserRepositoryAsync _userRepository;
		private readonly IMapper _mapper;

		public UpdateUserCommandHandler(IUserRepositoryAsync userRepository, IMapper mapper)
		{
			_userRepository = userRepository;
			_mapper = mapper;
		}

		public async Task<Response<long>> Handle(UpdateUserCommand command, CancellationToken cancellationToken)
		{
			var user = await _userRepository.GetByIdAsync(command.Id);

			if (user == null)
			{
				throw new ApiException($"User Not Found.");
			}
			else
			{
				user = _mapper.Map<User>(command);
				await _userRepository.UpdateAsync(user, command.RoleId);
				return new Response<long>(user.Id, "SucceededUpdate");
			}
		}
	}
}