﻿using Core.Application.Features.Roles.Queries.GetRoles;
using System;
using System.Collections.Generic;

namespace Core.Application.Features.Users.Queries.GetUsers
{
	public class GetUserDto
	{
		public long Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		public string MobileNumber { get; set; }
		public string PhoneNumber { get; set; }
		public byte Age { get; set; }
		public string Address { get; set; }
		public long RoleId { get; set; }
		public GetRoleDto Role { get; set; }
		public IEnumerable<long> StocksIds { get; set; }
	}
}