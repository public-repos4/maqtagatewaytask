﻿using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Application.Features.Users.Queries.GetUsers
{
	public class GetUsersQuery : QueryParameter, IRequest<PagedResponse<IEnumerable<Entity>>>
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string UserName { get; set; }
		public bool EmailConfirmed { get; set; }
	}

	public class GetAllUsersQueryHandler : IRequestHandler<GetUsersQuery, PagedResponse<IEnumerable<Entity>>>
	{
		private readonly IUserRepositoryAsync _userRepository;
		private readonly IModelHelper _modelHelper;
		private readonly IDataShapeHelper<GetUserDto> _dataShaper;
		private readonly IMapper _mapper;


		public GetAllUsersQueryHandler(
			IUserRepositoryAsync userRepository,
			IModelHelper modelHelper,
			IMapper mapper,
			IDataShapeHelper<GetUserDto> dataShaper)
		{
			_userRepository = userRepository;
			_modelHelper = modelHelper;
			_dataShaper = dataShaper;
			_mapper = mapper;
		}

		public async Task<PagedResponse<IEnumerable<Entity>>> Handle(GetUsersQuery queryParams, CancellationToken cancellationToken)
		{
			var validQueryParams = queryParams;

			//filtered fields security
			if (!string.IsNullOrEmpty(validQueryParams.Fields))
			{
				//limit to fields in view model
				validQueryParams.Fields = _modelHelper.ValidateModelFields<GetUserDto>(validQueryParams.Fields);
			}
			else
			{
				//default fields from view model
				validQueryParams.Fields = _modelHelper.GetModelFields<GetUserDto>();
			}

			// query based on filter
			var result = await _userRepository.GetPagedUserResponseAsync(validQueryParams);
			var data = _mapper.Map<IEnumerable<GetUserDto>>(result.data);
			RecordsCount recordCount = result.recordsCount;

			// shape data
			var shapeData = _dataShaper.ShapeData(data, validQueryParams.Fields);

			// response wrapper
			return new PagedResponse<IEnumerable<Entity>>(shapeData, validQueryParams.PageNumber, validQueryParams.PageSize, recordCount);
		}
	}
}