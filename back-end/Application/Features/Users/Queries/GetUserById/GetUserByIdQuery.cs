﻿using Core.Application.Exceptions;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Users.Queries.GetUserById
{
    public class GetUserByIdQuery : IRequest<Response<User>>
    {
        public long Id { get; set; }

        public class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, Response<User>>
        {
            private readonly IUserRepositoryAsync _userRepository;

            public GetUserByIdQueryHandler(IUserRepositoryAsync userRepository)
            {
                _userRepository = userRepository;
            }

            public async Task<Response<User>> Handle(GetUserByIdQuery queryParams, CancellationToken cancellationToken)
            {
                var user = await _userRepository.GetByIdAsync(queryParams.Id);
                if (user == null) throw new ApiException($"User Not Found.");
                return new Response<User>(user);
            }
        }
    }
}