﻿using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Application.Features.Roles.Queries.GetRoles
{
	public class GetRolesQuery : QueryParameter, IRequest<PagedResponse<IEnumerable<Entity>>>
	{
		public string Name { get; set; }
	}

	public class GetAllRolesQueryHandler : IRequestHandler<GetRolesQuery, PagedResponse<IEnumerable<Entity>>>
	{
		private readonly IRoleRepositoryAsync _roleRepository;
		private readonly IModelHelper _modelHelper;
		private readonly IDataShapeHelper<GetRoleDto> _dataShaper;
		private readonly IMapper _mapper;


		public GetAllRolesQueryHandler(
			IRoleRepositoryAsync roleRepository,
			IModelHelper modelHelper,
			IMapper mapper,
			IDataShapeHelper<GetRoleDto> dataShaper)
		{
			_roleRepository = roleRepository;
			_modelHelper = modelHelper;
			_dataShaper = dataShaper;
			_mapper = mapper;
		}

		public async Task<PagedResponse<IEnumerable<Entity>>> Handle(GetRolesQuery queryParams, CancellationToken cancellationToken)
		{
			var validQueryParams = queryParams;

			//filtered fields security
			if (!string.IsNullOrEmpty(validQueryParams.Fields))
			{
				//limit to fields in view model
				validQueryParams.Fields = _modelHelper.ValidateModelFields<GetRoleDto>(validQueryParams.Fields);
			}
			else
			{
				//default fields from view model
				validQueryParams.Fields = _modelHelper.GetModelFields<GetRoleDto>();
			}

			// query based on filter
			var result = await _roleRepository.GetPagedRoleResponseAsync(validQueryParams);
			var data = _mapper.Map<IEnumerable<GetRoleDto>>(result.data);
			RecordsCount recordCount = result.recordsCount;

			// shape data
			var shapeData = _dataShaper.ShapeData(data, validQueryParams.Fields);

			// response wrapper
			return new PagedResponse<IEnumerable<Entity>>(shapeData, validQueryParams.PageNumber, validQueryParams.PageSize, recordCount);
		}
	}
}