﻿using System;

namespace Core.Application.Features.Roles.Queries.GetRoles
{
    public class GetRoleDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}