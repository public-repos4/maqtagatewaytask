﻿using System.Collections.Generic;

namespace Core.Application.Wrappers
{
    public class Response<T>
    {
        //public Response(string message)
        //{
        //    Succeeded = false;
        //    Message = message;
        //}

        public bool Succeeded { get; set; }
        public string MessageKey { get; set; }
        public string Message { get; set; }
        public List<string> Errors { get; set; }
        public T Data { get; set; }

        public Response()
        {

        }

        public Response(T data, string messageKey = null, string message = null)
        {
            Succeeded = true;
            MessageKey = messageKey;
            Message = message;
            Data = data;
        }


    }
}