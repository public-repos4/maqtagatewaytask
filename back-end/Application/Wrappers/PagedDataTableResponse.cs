﻿using Core.Application.Parameters;

namespace Core.Application.Wrappers
{
    public class PagedDataTableResponse<T> : Response<T>
    {
        public int Draw { get; set; }
        public int RecordsFiltered { get; set; }
        public int RecordsTotal { get; set; }

        public PagedDataTableResponse(T data, int pageNumber, RecordsCount recordsCount)
        {
            this.Draw = pageNumber;
            this.RecordsFiltered = recordsCount.Filtered;
            this.RecordsTotal = recordsCount.Total;
            this.Data = data;
            this.Message = null;
            this.Succeeded = true;
            this.Errors = null;
        }
    }
}