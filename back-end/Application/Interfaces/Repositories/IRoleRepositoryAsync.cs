﻿using Core.Application.Features.Roles.Queries.GetRoles;
using Core.Application.Parameters;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Application.Interfaces.Repositories
{
    public interface IRoleRepositoryAsync : IGenericRepositoryAsync<Role>
    {
        Task<bool> IsExistedRoleIdAsync(long roleId);

        Task<(IEnumerable<Role> data, RecordsCount recordsCount)> GetPagedRoleResponseAsync(GetRolesQuery queryParams);
    }
}