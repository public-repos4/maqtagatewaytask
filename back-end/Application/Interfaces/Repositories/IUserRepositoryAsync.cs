﻿using Core.Application.Features.Users.Queries.GetUsers;
using Core.Application.Parameters;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Application.Interfaces.Repositories
{
	public interface IUserRepositoryAsync : IGenericRepositoryAsync<User>
	{
		Task<bool> IsUniqueUserNameAsync(string userName, long? userId = null);

		Task<User> AddAsync(User user, long roleId);

		Task<(IEnumerable<User> data, RecordsCount recordsCount)> GetPagedUserResponseAsync(GetUsersQuery queryParams);

		Task<User> UpdateAsync(User user, long roleId);
	}
}