﻿using AutoMapper;
using Core.Application.Features.Users.Queries.GetUsers;
using Core.Domain.Entities.Identity;
using Core.Application.Features.Users.Commands.CreateUser;
using Core.Application.Features.Users.Commands.UpdateUser;
using Core.Application.Features.Roles.Queries.GetRoles;
using System.Linq;

namespace Core.Application.Mappings
{
	public class GeneralProfile : Profile
	{
		public GeneralProfile()
		{
			CreateMap<CreateUserCommand, User>();
			CreateMap<User, GetUserDto>()
				.ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.UserRoles.Select(userRole => userRole.Role).FirstOrDefault()))
				.ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.UserRoles.Select(userRole => userRole.Role).First().Id));
			CreateMap<UpdateUserCommand, User>();

			CreateMap<Role, GetRoleDto>();
		}
	}
}