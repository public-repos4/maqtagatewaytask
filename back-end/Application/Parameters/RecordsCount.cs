﻿
namespace Core.Application.Parameters
{
    public class RecordsCount
    {
        public int Filtered { get; set; }
        public int Total { get; set; }
    }
}