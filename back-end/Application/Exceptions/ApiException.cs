﻿using System;
using System.Globalization;

namespace Core.Application.Exceptions
{
    public class ApiException : Exception
    {
		public string Message { get; set; }
		public string MessageKey { get; set; }

		public ApiException() : base()
        {
        }

        public ApiException(string message) : base(message)
        {
        }

        public ApiException(string message, string messageKey)
        {
            this.MessageKey = messageKey;
            this.Message = message;
        }

        public ApiException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args))
        {
        }

        public ApiException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}