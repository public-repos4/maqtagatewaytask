﻿using Core.Application.Features.Users.Commands.CreateUser;
using Core.Application.Features.Users.Commands.DeleteUserById;
using Core.Application.Features.Users.Commands.UpdateUser;
using Core.Application.Features.Users.Queries.GetUserById;
using Core.Application.Features.Users.Queries.GetUsers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Presentation.WebApi.Controllers.v1
{
	[ApiVersion("1.0")]
	public class UsersController : BaseApiController
	{
		/// <summary>
		/// POST: api/v1/[controller]
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		/// 
		[HttpPost]
		public async Task<IActionResult> Post(CreateUserCommand command)
		{
			return Ok(await Mediator.Send(command));
		}

		/// <summary>
		/// GET: api/v1/[controller]/{id}
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet("{id}")]
		public async Task<IActionResult> Get(long id)
		{
			return Ok(await Mediator.Send(new GetUserByIdQuery { Id = id }));
		}

		/// <summary>
		/// GET: api/[controller]
		/// </summary>
		/// <param name="queryParams"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<IActionResult> Get([FromQuery] GetUsersQuery queryParams)
		{
			return Ok(await Mediator.Send(queryParams));
		}

		/// <summary>
		/// GET: api/v1/[controller]/GetDatatable
		/// </summary>
		/// <param name="dtQueryParams"></param>
		/// <returns></returns>
		[HttpPost]
		[Route("GetDatatable")]
		public async Task<IActionResult> GetDatatable([FromForm] PagedUsersQuery dtQueryParams)
		{
			return Ok(await Mediator.Send(dtQueryParams));
		}

		/// <summary>
		/// PUT: api/v1/[controller]/{id}
		/// </summary>
		/// <param name="id"></param>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut("{id}")]
		public async Task<IActionResult> Put(long id, UpdateUserCommand command)
		{
			if (id != command.Id)
			{
				return BadRequest();
			}

			return Ok(await Mediator.Send(command));
		}

		/// <summary>
		/// DELETE: api/v1/[controller]/{id}
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(long id)
		{
			return Ok(await Mediator.Send(new DeleteUserByIdCommand { Id = id }));
		}
	}
}