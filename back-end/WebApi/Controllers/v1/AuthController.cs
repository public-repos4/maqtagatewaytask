﻿using Core.Domain.Entities.Identity;
using Presentation.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using Presentation.WebApi.Controllers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Core.Application.Features.Auth.Commands.Login;

namespace Presentation.WebApi.Controllers.v1
{
	[ApiVersion("1.0")]
	public class AuthController : BaseApiController
	{
		private readonly IConfiguration _config;
		private readonly UserManager<User> _usersManager;
		private readonly SignInManager<User> _signInManager;

		public AuthController(IConfiguration config, UserManager<User> usersManager, SignInManager<User> signInManager)
		{
			_usersManager = usersManager;
			_signInManager = signInManager;
			_config = config;
		}

		[HttpPost]
		[AllowAnonymous]
		[Route("Login")]
		public async Task<IActionResult> Login(LoginCommand command)
		{
			return Ok(await Mediator.Send(command));
		}
	}
}
