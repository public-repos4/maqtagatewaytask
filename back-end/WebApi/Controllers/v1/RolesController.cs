﻿using Core.Application.Features.Roles.Queries.GetRoles;
using Presentation.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Presentation.WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class RolesController : BaseApiController
    {
        /// <summary>
        /// GET: api/[controller]
        /// </summary>
        /// <param name="queryParams"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetRolesQuery queryParams)
        {
            return Ok(await Mediator.Send(queryParams));
        }
    }
}