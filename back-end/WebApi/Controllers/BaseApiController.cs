﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Threading;

namespace Presentation.WebApi.Controllers
{
	[ApiController]
	[Route("api/v{version:apiVersion}/[controller]")]
	public abstract class BaseApiController : ControllerBase
	{
		private IMediator _mediator;
		protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
		protected readonly IHttpContextAccessor _httpContextAccessor;

		public BaseApiController(IHttpContextAccessor httpContextAccessor = null)
		{
			_httpContextAccessor = httpContextAccessor;
		}

		protected string GetCurrentCultureName(IHeaderDictionary headers)
		{
			List<string> allowdCultures = new() { "en", "ar" };

			string currentCultureName = Thread.CurrentThread.CurrentCulture.Name;

			string culture = allowdCultures.Contains(currentCultureName)
				? currentCultureName
				: "en";

			if (headers.ContainsKey("Accept-Language"))
			{

				if (allowdCultures.Contains(headers["Accept-Language"].ToString()))
				{
					culture = headers["Accept-Language"].ToString();
				}
			}

			return culture;
		}
	}
}