using Core.Application;
using Infrastructure.Persistence;
using Infrastructure.Shared;
using Presentation.WebApi.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using static Presentation.WebApi.Extensions.AppExtensions;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.WebApi
{
	public class Startup
	{
		public IConfiguration _config { get; }

		public Startup(IConfiguration configuration)
		{
			_config = configuration;
		}

		public void ConfigureServices(IServiceCollection services)
		{
			services.Configure<ApiBehaviorOptions>(options =>
			{
				options.SuppressModelStateInvalidFilter = true;
			});

			services.AddApplicationLayer();
			services.AddPersistenceInfrastructure(_config);
			services.AddSharedInfrastructure(_config);

			services.AddSwaggerExtension();
			services.AddControllersExtension();

			// CORS
			services.AddCorsExtension();
			services.AddHealthChecks();

			// API version
			services.AddApiVersioningExtension();

			// API explorer version
			services.AddVersionedApiExplorerExtension();

			// API Security
			services.AddJWTAuthentication(_config);
			services.AddGlobalAuthorization();

			// API Identity
			services.AddIdentity();
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
		{
			//app.UseDeveloperExceptionPage();

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Error");
				app.UseHsts();
			}

			// Add this line; you'll need `using Serilog;` up the top, too
			app.UseSerilogRequestLogging();
			loggerFactory.AddSerilog();

			#region Https redirection
			app.UseHttpsRedirection();
			#endregion

			#region Static files
			app.UseStaticFiles();
			#endregion

			#region Routing
			// Matches request to an endpoint.
			app.UseRouting();
			#endregion

			#region CORS (Configures Cross-Origin Resource Sharing)
			// global cors policy
			app.UseCors(x => x
				.AllowAnyMethod()
				.AllowAnyHeader()
				.SetIsOriginAllowed(origin => true) // allow any origin
				.AllowCredentials()); // allow credentials
			#endregion

			#region Authentication
			// Cheking if the user is exist
			app.UseAuthentication();
			#endregion

			#region Authorization
			// Cheking if the user has a valid role to the app resources
			app.UseAuthorization();
			#endregion

			app.UseSwaggerExtension();

			app.UseErrorHandlingMiddleware();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}