﻿using Presentation.WebApi.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace Presentation.WebApi.Extensions
{
    public static class AppExtensions
    {
        public static void UseSwaggerExtension(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
				c.SwaggerEndpoint("../swagger/v1/swagger.json", "Maqta Gateway Api - v1");
                c.SwaggerEndpoint("../swagger/v2/swagger.json", "Maqta Gateway Api - v2");
            });
        }

        public static void UseErrorHandlingMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandlerMiddleware>();
        }
    }
}