﻿using System;

namespace Core.Domain.Common
{
    public abstract class AuditableBaseEntity : BaseEntity
    {
        public long CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public long LastModifiedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
    }
}