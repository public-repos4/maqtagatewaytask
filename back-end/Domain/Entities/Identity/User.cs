﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Core.Domain.Entities.Identity
{
    public class User : IdentityUser<long>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string MobileNumber { get; set; }
        public byte Age { get; set; }
        public string Address { get; set; }

		public virtual ICollection<UserRole> UserRoles { get; set; }
	}
}
