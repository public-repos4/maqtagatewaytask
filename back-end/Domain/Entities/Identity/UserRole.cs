﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Core.Domain.Entities.Identity
{
    public class UserRole : IdentityUserRole<long>
    {
        public virtual User User { get; set; }
        public virtual Role Role { get; set; }
    }
}
