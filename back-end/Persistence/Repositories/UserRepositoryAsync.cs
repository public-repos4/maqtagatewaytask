﻿using Core.Application.Features.Users.Queries.GetUsers;
using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using Infrastructure.Persistence.Contexts;
using LinqKit;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repositories
{
	public class UserRepositoryAsync : GenericRepositoryAsync<User>, IUserRepositoryAsync
	{
		private readonly AppDbContext _dbContext;
		private readonly DbSet<User> _users;
		private readonly UserManager<User> _usersManager;
		private readonly RoleManager<Role> _rolesManager;

		public UserRepositoryAsync(
			AppDbContext dbContext,
			UserManager<User> usersManager,
			RoleManager<Role> rolesManager
			) : base(dbContext)
		{
			_dbContext = dbContext;
			_users = dbContext.Set<User>();
			_usersManager = usersManager;
			_rolesManager = rolesManager;
		}

		public async Task<bool> IsUniqueUserNameAsync(string userName, long? userId = null)
		{
			if (userId != null)
			{
				return await _users
					.AllAsync(p => ((p.Id == userId && p.UserName.ToLower() == userName.ToLower()) || p.UserName.ToLower() != userName.ToLower()));
			}

			else
			{
				return await _users
					.AllAsync(p => p.UserName.ToLower() != userName.ToLower());
			}
		}

		public async Task<User> AddAsync(User user, long roleId)
		{
			// Create user
			if (!string.IsNullOrEmpty(user.Password))
			{
				await _usersManager.CreateAsync(user, user.Password);
			}
			else
			{
				await _usersManager.CreateAsync(user, user.UserName);
			}

			user = _usersManager.FindByNameAsync(user.UserName).Result;

			// To prevent throwing tracking exception on addin roles to this user (user)
			_dbContext.ChangeTracker.Entries<User>().Where(x => x.Entity.Id == user.Id).First().State = EntityState.Detached;

			// Add user to role
			Role role = _rolesManager.FindByIdAsync(roleId.ToString()).Result;
			await _usersManager.AddToRolesAsync(user, new[] { role.Name });
			return user;
		}

		public async Task<(IEnumerable<User> data, RecordsCount recordsCount)> GetPagedUserResponseAsync(GetUsersQuery queryParams)
		{
			var orderBy = queryParams.OrderBy;
			int recordsTotal, recordsFiltered;

			var query = (IQueryable<User>)_usersManager.Users.Include(user => user.UserRoles).ThenInclude(userRole => userRole.Role);

			// Count records total
			recordsTotal = await query.CountAsync();

			// filter data
			query = FilterData(query, queryParams);

			// Count records after filter
			recordsFiltered = await query.CountAsync();

			//set Record counts
			var recordsCount = new RecordsCount
			{
				Filtered = recordsFiltered,
				Total = recordsTotal
			};

			// set order by
			if (!string.IsNullOrWhiteSpace(orderBy))
			{
				query = query.OrderBy(orderBy);
			}

			// paginate data
			query = PaginateData(query, queryParams.PageNumber, queryParams.PageSize);

			//retrieve data to list
			var resultData = await query.ToListAsync();

			return (resultData, recordsCount);
		}

		private static IQueryable<User> FilterData(IQueryable<User> users, GetUsersQuery queryParams)
		{
			bool isForConfig = queryParams.IsForConfig;

			var id = queryParams.Id;
			var ids = queryParams.Ids;
			var search = queryParams.Search?.Trim();

			var firstName = queryParams.FirstName?.Trim();
			var lastName = queryParams.LastName?.Trim();
			var userName = queryParams.UserName?.Trim();
			var emailConfirmed = queryParams.EmailConfirmed;

			var predicate = PredicateBuilder.New<User>(true);

			if (!string.IsNullOrEmpty(id.ToString()))
			{
				predicate = predicate.Or(entity => entity.Id == id);
				return users.Where(predicate);
			}

			if (ids != null)
			{
				predicate = predicate.Or(entity => ids.Contains(entity.Id));
				return users.Where(predicate);
			}

			if (!string.IsNullOrEmpty(search))
			{
				predicate = predicate
					.Or(entity => entity.FirstName.Contains(search));

				predicate = predicate
					.Or(entity => entity.LastName.Contains(search));

				//predicate = predicate
				//	.Or(entity => entity.UserRoles.ElementAt(0).Role.Name.ToString().Contains(search));

				//predicate = predicate
				//	.Or(entity => ((List<UserRole>)entity.UserRoles).ElementAt(0).Role.Name.ToString().Contains(search));

				predicate = predicate
					.Or(entity => entity.Email.Contains(search));

				predicate = predicate
					.Or(entity => entity.MobileNumber.Contains(search));

				predicate = predicate
					.Or(entity => entity.PhoneNumber.Contains(search));

				predicate = predicate
					.Or(entity => entity.Age.ToString().Contains(search));

				predicate = predicate
					.Or(entity => entity.Address.Contains(search));
			}

			if (!string.IsNullOrEmpty(firstName))
				predicate = predicate.Or(entity => entity.FirstName.Contains(firstName));

			if (!string.IsNullOrEmpty(lastName))
				predicate = predicate.Or(entity => entity.LastName.Contains(lastName));

			if (!string.IsNullOrEmpty(userName))
				predicate = predicate.Or(entity => entity.UserName.Contains(userName));

			if (emailConfirmed)
				predicate = predicate.Or(entity => entity.EmailConfirmed);

			users = users.Where(predicate);

			return users;
		}

		public async Task<User> UpdateAsync(User user, long roleId)
		{
			User originUser = await _usersManager.FindByIdAsync(user.Id.ToString());

			// Update passowrd
			if (!string.IsNullOrEmpty(user.Password))
			{
				var result = await _usersManager.ChangePasswordAsync(originUser, originUser.Password, user.Password);

				if (result.Succeeded)
				{
					originUser.Password = user.Password;
				}
			}

			// Update Properties
			originUser.FirstName = user.FirstName;
			originUser.LastName = user.LastName;
			originUser.UserName = user.UserName;

			// Update user
			await _usersManager.UpdateAsync(originUser);

			originUser = await _usersManager.FindByIdAsync(originUser.Id.ToString());

			// Remove old role
			var roles = await _usersManager.GetRolesAsync(originUser);
			await _usersManager.RemoveFromRolesAsync(originUser, roles.ToArray());

			// Add a new role
			Role role = _rolesManager.FindByIdAsync(roleId.ToString()).Result;
			await _usersManager.AddToRolesAsync(originUser, new[] { role.Name });

			return await _usersManager.FindByIdAsync(originUser.Id.ToString());
		}
	}
}