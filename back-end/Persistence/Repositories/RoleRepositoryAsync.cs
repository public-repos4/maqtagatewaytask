﻿using Core.Application.Features.Roles.Queries.GetRoles;
using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using Infrastructure.Persistence.Contexts;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repositories
{
	public class RoleRepositoryAsync : GenericRepositoryAsync<Role>, IRoleRepositoryAsync
	{
		private readonly DbSet<Role> _roles;

		public RoleRepositoryAsync(
			AppDbContext dbContext
		) : base(dbContext)
		{
			_roles = dbContext.Set<Role>();
		}

		public async Task<bool> IsExistedRoleIdAsync(long roleId)
		{
			return await _roles
				.AnyAsync(role => role.Id == roleId);
		}

		public async Task<(IEnumerable<Role> data, RecordsCount recordsCount)> GetPagedRoleResponseAsync(GetRolesQuery queryParams)
		{
			var orderBy = queryParams.OrderBy;
			int recordsTotal, recordsFiltered;

			// Setup IQueryable
			var query = _roles
				.AsNoTracking()
				.AsExpandable();

			// Count records total
			recordsTotal = await query.CountAsync();

			// filter data
			query = FilterData(query, queryParams);

			// Count records after filter
			recordsFiltered = await query.CountAsync();

			//set Record counts
			var recordsCount = new RecordsCount
			{
				Filtered = recordsFiltered,
				Total = recordsTotal
			};

			// set order by
			if (!string.IsNullOrWhiteSpace(orderBy))
			{
				query = query.OrderBy(orderBy);
			}

			// paginate data
			query = PaginateData(query, queryParams.PageNumber, queryParams.PageSize);

			//retrieve data to list
			var resultData = await query.ToListAsync();

			// shape data
			//var shapeData = _dataShaper.ShapeData(resultData, fields);

			return (resultData, recordsCount);
		}

		private static IQueryable<Role> FilterData(IQueryable<Role> roles, GetRolesQuery queryParams)
		{
			bool isForConfig = queryParams.IsForConfig;

			var id = queryParams.Id;
			var ids = queryParams.Ids;
			var search = queryParams.Search?.Trim();

			var name = queryParams.Name?.Trim();

			var predicate = PredicateBuilder.New<Role>(true);

			if (!string.IsNullOrEmpty(id.ToString()))
			{
				predicate = predicate.Or(entity => entity.Id == id);
				return roles.Where(predicate);
			}

			if (ids != null)
			{
				predicate = predicate.Or(entity => ids.Contains(entity.Id));
				return roles.Where(predicate);
			}

			if (!string.IsNullOrEmpty(search))
			{
				predicate = predicate.Or(entity => entity.Name.Contains(search));
			}

			if (!string.IsNullOrEmpty(name))
				predicate = predicate.Or(entity => entity.Name.Contains(name));

			roles = roles.Where(predicate);

			return roles;
		}
	}
}