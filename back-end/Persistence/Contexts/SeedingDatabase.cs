﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Security.Claims;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using Bogus;

namespace Infrastructure.Persistence.Contexts
{
	public class SeedingDatabase
	{
		public static Faker<User> UserFaker;

		public async static Task Seed(IServiceProvider serviceProvider)
		{
			var appDbContext = serviceProvider.GetRequiredService<AppDbContext>();
			var usersManager = serviceProvider.GetRequiredService<UserManager<User>>();
			var rolesManager = serviceProvider.GetRequiredService<RoleManager<Role>>();

			try
			{
				appDbContext.Database.OpenConnection();
				//appDbContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.DestuffedContainer ON");

				if (!rolesManager.Roles.Any())
				{
					var roles = new List<Role>
					{
						new Role{Name = "Admin"},
						new Role{Name = "Employee"},
					};

					foreach (var role in roles)
					{
						await rolesManager.CreateAsync(role);
					}
				}

				if (!usersManager.Users.Any())
				{
					var wajeehUser = new User
					{
						FirstName = "Wajeeh",
						LastName = "Abiad",
						UserName = "wajeeh",
						Password = "wajeeh",
						Email = "wajeeh@task.org",
						PhoneNumber = "+971 56618 4663",
						MobileNumber = "+971 56618 4663",
						Age = 28,
						Address = "UAE, Sharja",
					};

					IdentityResult result = usersManager.CreateAsync(wajeehUser, wajeehUser.Password).Result;

					if (result.Succeeded)
					{
						var wajeeh = usersManager.FindByNameAsync(wajeehUser.UserName).Result;

						// To prevent throwing tracking exception on addin roles to this user (user)
						appDbContext.ChangeTracker.Entries<User>().Where(x => x.Entity.Id == wajeeh.Id).First().State = EntityState.Detached;

						await usersManager.AddToRolesAsync(wajeeh, new[] { "Employee" });
					}

					UserFaker = new Faker<User>()
						//Basic rules using built-in generators
						.RuleFor(u => u.FirstName, (f, u) => f.Person.FirstName)
						.RuleFor(u => u.LastName, (f, u) => f.Person.LastName)
						.RuleFor(u => u.UserName, (f, u) => f.Person.UserName)
						.RuleFor(u => u.Password, (f, u) => f.Internet.Password())
						.RuleFor(u => u.MobileNumber, (f, u) => f.Phone.PhoneNumber())
						.RuleFor(u => u.PhoneNumber, (f, u) => f.Phone.PhoneNumber())
						.RuleFor(u => u.Address, (f, u) => f.Address.FullAddress())
						.RuleFor(u => u.Age, (f, u) => (byte)f.Random.Number(20, 60))
						.RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FirstName, u.LastName))
						.RuleFor(u => u.EmailConfirmed, (f, u) => f.Random.Bool());

					Random random = new();

					var roles = appDbContext.Roles.ToList();

					foreach (var entity in UserFaker.Generate(50))
					{
						result = usersManager.CreateAsync(entity, entity.Password).Result;

						if (result.Succeeded)
						{
							var user = usersManager.FindByNameAsync(entity.UserName).Result;

							// To prevent throwing tracking exception on addin roles to this user (user)
							appDbContext.ChangeTracker.Entries<User>().Where(x => x.Entity.Id == user.Id).First().State = EntityState.Detached;

							await usersManager.AddToRolesAsync(user, new[] { roles[random.Next(roles.Count)].Name });
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				appDbContext.Database.CloseConnection();
			}
		}
	}
}
