﻿using Core.Application.Interfaces;
using Core.Domain.Common;
using Core.Domain.Entities;
using Core.Domain.Entities.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Contexts
{
	//public class AppDbContext : IdentityDbContext
	public class AppDbContext :
		IdentityDbContext<User, Role, long,
		UserClaim, UserRole, UserLogin,
		RoleClaim, UserToken>
	{
		private readonly IDateTimeService _dateTime;
		private readonly ILoggerFactory _loggerFactory;
		protected readonly IHttpContextAccessor _httpContextAccessor;

		public AppDbContext(DbContextOptions<AppDbContext> options,
		  IDateTimeService dateTime,
		  ILoggerFactory loggerFactory,
		  IHttpContextAccessor httpContextAccessor = null
		  ) : base(options)
		{
			ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking; // Must be commented on seeding the database
			_dateTime = dateTime;
			_loggerFactory = loggerFactory;
			_httpContextAccessor = httpContextAccessor;
		}

		public DbSet<User> Users { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);

			builder.Entity<UserRole>(userRole =>
			{
				userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

				userRole.HasOne(ur => ur.Role)
					.WithMany(r => r.RoleUsers)
					.HasForeignKey(ur => ur.RoleId)
					.IsRequired();

				userRole.HasOne(ur => ur.User)
					.WithMany(r => r.UserRoles)
					.HasForeignKey(ur => ur.UserId)
					.IsRequired();
			});

			#region Entities


			#region Users
			builder.Entity<User>(entity =>
			{
				entity.ToTable("Users");
			});
			#endregion

			#region Roles
			builder.Entity<Role>(entity =>
			{
				entity.ToTable("Roles");
			});
			#endregion

			#region UsersRoles
			builder.Entity<UserRole>(entity =>
			{
				entity.ToTable("UsersRoles");
			});
			#endregion

			#region UserLogins
			builder.Entity<UserLogin>(entity =>
			{
				entity.ToTable("UserLogins");
			});
			#endregion

			#region UserTokens
			builder.Entity<UserToken>(entity =>
			{
				entity.ToTable("UserTokens");
			});
			#endregion

			#region UserClaims
			builder.Entity<UserClaim>(entity =>
			{
				entity.ToTable("UserClaims");
			});
			#endregion

			#region RolesClaims
			builder.Entity<RoleClaim>(entity =>
			{
				entity.ToTable("RolesClaims");
			});
			#endregion


			#endregion

			#region Views
			#endregion
		}

		public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
		{
			var x = ChangeTracker.Entries<AuditableBaseEntity>();

			foreach (var entry in ChangeTracker.Entries<AuditableBaseEntity>())
			{
				switch (entry.State)
				{
					case EntityState.Added:
						entry.Entity.CreatedAt = _dateTime.NowUtc;
						break;

					case EntityState.Modified:
						entry.Entity.LastModifiedAt = _dateTime.NowUtc;
						break;
				}
			}
			return base.SaveChangesAsync(cancellationToken);
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseLoggerFactory(_loggerFactory);
		}
	}
}