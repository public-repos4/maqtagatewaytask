$(document).ready(function () {
    //Initialize input[type="checkbox"] as switch
    $('.switch-checkbox').bootstrapSwitch({
        onSwitchChange: function (e, state) {
            $(this).val(state);
        }
    });
});