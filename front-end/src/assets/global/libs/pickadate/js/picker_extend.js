﻿$(document).ready(function () {
    $('.date-picker').pickadate({
        //format: 'd mmmm, yyyy',
        //format: 'yyyy-mm-dd',
        //format: 'dd/mm/yyyy',
        //format: 'dd-mm-yyyy',
        format: 'yyyy-mm-dd',
        formatSubmit: 'yyyy-mm-dd',
        hiddenglishName: true,

        //monthsFull: ['يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر'],
        //monthsShort: ['يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر'],
        //weekdaysFull: ['الاحد', 'الاثنين', 'الثلاثاء', 'الاربعاء', 'الخميس', 'الجمعة', 'السبت'],
        //weekdaysShort: ['الاحد', 'الاثنين', 'الثلاثاء', 'الاربعاء', 'الخميس', 'الجمعة', 'السبت'],
        //today: 'اليوم',
        //clear: 'مسح',
        //close: 'إغلاق',
    });

    $('.time-picker').pickatime({
        format: 'hh:i A',
        formatSubmit: 'HH:i',
        hiddenglishName: true,
        //container: '#body',
        //containerHidden: '#body'
        //min: [7, 30],
        //max: [14, 0]
    });

    durationFormat = language == 'ar' ? 'HH ساعة و i دقيقة' : 'HH !hours !and i m!inutes'

    $('.duration-picker').pickatime({
        format: durationFormat,
        formatSubmit: 'HH:i',
        hiddenglishName: true,
        formatLabel: durationFormat
    });
});

