const prevClass = direction == 'ltr' ? 'slick-prev' : 'slick-next'
const prevIcon = direction == 'ltr' ? 'icon-arrow-left22' : 'icon-arrow-right22'

const nextClass = direction == 'ltr' ? 'slick-next' : 'slick-prev'
const nextIcon = direction == 'ltr' ? 'icon-arrow-right22' : 'icon-arrow-left22'

function initSlickMenu($menu) {

    $menu.slick({
        dots: false,
        infinite: false,
        speed: 800,
        //autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 4,
        slidesToScroll: 1,
        //accessibility: false,
        draggable: false,

        rtl: direction == 'rtl' ? true : false,

        prevArrow: '<button class="btn bg-dark text-white btn-icon slide-arrow slick-prev"><i class="' + prevIcon + ' icon-2x"></i></button>',
        nextArrow: '<button class="btn bg-dark text-white btn-icon slide-arrow slick-next"><i class="' + nextIcon + ' icon-2x"></i></button>',

        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }

        ]
    });

    $menu.find('.slick-prev').addClass('d-none')

    $menu.on('afterChange', function (event, slick, currentSlide) {

        let totalSlides = $(this).find('.slick-slide').length;
        let firstSlideIndex = 0;
        let lastSlideIndex = totalSlides - 1;
        console.log(lastSlideIndex);

        let $firstSlide = $(this).find('.slick-slide[data-slick-index="' + firstSlideIndex + '"]');
        let $lastSlide = $(this).find('.slick-slide[data-slick-index="' + lastSlideIndex + '"]');

        if ($firstSlide.hasClass('slick-active')) {
            $(this).find('.slick-prev').addClass('d-none')
        }
        else {
            $(this).find('.slick-prev').removeClass('d-none')
        }

        if ($lastSlide.hasClass('slick-active')) {
            $(this).find('.slick-next').addClass('d-none')
        }
        else {
            $(this).find('.slick-next').removeClass('d-none')
        }
    })
}