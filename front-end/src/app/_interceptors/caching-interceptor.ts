import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpEvent, HttpRequest, HttpResponse, HttpHandler} from '@angular/common/http';
import {of} from 'rxjs';
import {tap} from 'rxjs/operators';
import {CacheService} from '../_services/cache.service';

@Injectable()
export class CachingInterceptor implements HttpInterceptor {

    constructor(
        private cache: CacheService
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        if (req.method === 'POST' || req.method === 'PUT' || req.method === 'DELETE') {
            this.cache.removeAllCachedRequestsForCustomController(req)
        }

        const cachedResponse = this.cache.get(req);
        if (cachedResponse !== null) {
            return of(cachedResponse);
        }
        return next.handle(req).pipe(
            tap(event => {
                if (event instanceof HttpResponse) {
                    this.cache.put(req, event);
                }
            })
        );
    }
}

