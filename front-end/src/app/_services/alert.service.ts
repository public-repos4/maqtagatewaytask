﻿import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {filter} from 'rxjs/operators';

import {Alert, AlertType} from 'src/app/_models';

@Injectable({providedIn: 'root'})
export class AlertService {
    constructor() {
    }

    private subject = new Subject<Alert>();
    private defaultId = 'default-alert';
    private lang: string = 'en';

    // enable subscribing to alerts observable
    onAlert(id = this.defaultId): Observable<Alert> {
        return this.subject.asObservable().pipe(filter(x => x && x.id === id));
    }

    // convenience methods
    success(messageKey: string, options?: any) {
        this.alert(new Alert({...options, type: AlertType.Success, message: this.getSuccessMessage(messageKey)}));
    }

    error(messageKey: string, options?: any) {
        this.alert(new Alert({...options, type: AlertType.Error, message: this.getErrorMessage(messageKey)}));
    }

    info(message: string, options?: any) {
        this.alert(new Alert({...options, type: AlertType.Info, message}));
    }

    warn(message: string, options?: any) {
        this.alert(new Alert({...options, type: AlertType.Warning, message}));
    }

    // main alert method
    alert(alert: Alert) {
        alert.id = alert.id || this.defaultId;
        this.subject.next(alert);
    }

    // clear alerts
    clear(id = this.defaultId) {
        this.subject.next(new Alert({id}));
    }

    getSuccessMessage(messageKey: string) {
        switch (messageKey) {
            case 'SucceededAdd':
                return this.lang == 'ar' ? 'تمت الإضافة بنجاح' : 'Created successfully';

            case 'SucceededEdit':
                return this.lang == 'ar' ? 'تم التعديل بنجاح' : 'Updated successfully';

            case 'SucceededDelete':
                return this.lang == 'ar' ? 'تم الحذف بنجاح' : 'Deleted successfully';

            case 'SoonActivatingAccount':
                return this.lang == 'ar' ? 'سيتم تفعيل حسابك قريبا' : 'Your account will be activated soon';

            default:
                return messageKey
        }
    }

    getErrorMessage(messageKey: string) {
        switch (messageKey) {
            case 'ConflictedWithReferenceConstraint':
                return this.lang == 'ar'
                    ? 'لا يمكن حذف هذا العنصر لارتباطه ببيانات أخرى'
                    : 'You can not delete this item to its association with other data';

            default:
                return messageKey
        }
    }
}
