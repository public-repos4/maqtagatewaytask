﻿import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {delay, map} from 'rxjs/operators';

import {environment} from 'src/environments/environment';
import {Role} from 'src/app/_models/role';
import {SelectOption} from "../_models/select-option";

@Injectable({providedIn: 'root'})
export class RolesService {
    private roleSubject: BehaviorSubject<Role>;
    public role: Observable<Role>;

    constructor(
        private router: Router,
        private http: HttpClient,
    ) {
        this.roleSubject = new BehaviorSubject<Role>(JSON.parse(localStorage.getItem('role')));
        this.role = this.roleSubject.asObservable();
    }

    add(role: Role) {
        return this.http.post(`${environment.apiUrl}/roles`, role).pipe();
    }

    getAll(params: HttpParams) {
        return this.http.get<Role[]>(`${environment.apiUrl}/roles`, {params: params})
            .pipe(map(response => response['data']));
    }

    getById(id: number) {
        return this.getAll(new HttpParams({fromObject: {id: id}}))
            .pipe(map(data => data[0]));
    }

    getRolesSelect(term: string = ''): Observable<SelectOption[]> {
        return this.getAll(new HttpParams({fromObject: {name: term, pageSize: 2000}}))
            .pipe(map(data =>  data.map(item => ({id: item.id, text: item.name} as SelectOption))))
    }

    edit(id, role: Role) {
        return this.http.put(`${environment.apiUrl}/roles/${id}`, role).pipe();
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/roles/${id}`).pipe();
    }
}
