﻿import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from 'src/environments/environment';

import {User} from 'src/app/_models/user/user';
import {map} from "rxjs/operators";
import {ApiResponse} from "../_models/api-response";
import {UserSubmit} from "../_models/user/user-submit";

@Injectable({providedIn: 'root'})
export class UsersService {
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

    constructor(
        private router: Router,
        private http: HttpClient,
    ) {
        this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
        this.user = this.userSubject.asObservable();
    }

    add(user: UserSubmit) {
        return this.http.post(`${environment.apiUrl}/users`, user).pipe();
    }

    getAll(queryParams?: HttpParams) {
        return this.http.get<ApiResponse>(`${environment.apiUrl}/users`, {params: queryParams})
            .pipe(map(response => response));
    }

    getById(id: number) {
        return this.getAll(new HttpParams({fromObject: {id: id}}))
            .pipe(map(response => response.data[0]));
    }

    edit(id, user: UserSubmit) {
        return this.http.put(`${environment.apiUrl}/users/${id}`, user).pipe();
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/${id}`).pipe();
    }
}
