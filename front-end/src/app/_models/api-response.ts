﻿export class ApiResponse {
    data: any[];
    errors: any;
    message: string | null;
    pageNumber: number;
    pageSize: number;
    recordsFiltered: number;
    recordsTotal: number;
    succeeded: boolean
}
