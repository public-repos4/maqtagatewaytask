﻿
export class UserRegister {
    id?: number;
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    age: number;
    phoneNumber: string;
    mobileNumber: string;
    address: string;
    password: string;
}
