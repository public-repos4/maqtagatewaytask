﻿
export class UserSubmit {
    id?: number;
    firstName: string;
    lastName: string;
    userName: string;
    password: string;
    email: string;
    age: number;
    phoneNumber: string;
    mobileNumber: string;
    address: string;
    isActive: boolean
}
