﻿export class User {
    id: number;
    title: string;
    imageBefore: string;
    imageAfter: string;
    imageAfter2: string;
    creationDate: string;
}
