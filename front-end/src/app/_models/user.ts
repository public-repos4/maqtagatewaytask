﻿export class User {
    id: string;
    userName: string;
    password: string;
    firstName: string;
    lastName: string;
    role: string;
    token: string;
}
