import {Component, OnInit} from '@angular/core';
import {Router, NavigationStart, NavigationCancel, NavigationEnd} from '@angular/router';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {filter} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

declare let $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    providers: [
        Location, {
            provide: LocationStrategy,
            useClass: PathLocationStrategy
        }
    ]
})
export class AppComponent implements OnInit {

    constructor(
        private router: Router,
        public translate: TranslateService,
    ) {
    };

    location: any;
    routerSubscription: any;

    ngOnInit() {
        this.recallJsFunctions();
    }

    recallJsFunctions() {
        // this.router.events
        //     .subscribe((event) => {
        //         if (event instanceof NavigationStart) {
        //             $('.loader-content').fadeIn('slow');
        //         }
        //     });
        this.routerSubscription = this.router.events
            .pipe(filter(event => event instanceof NavigationEnd || event instanceof NavigationCancel))
            .subscribe(event => {
                $.getScript('../assets/global/js/custom.js');
                $('.loader-content').fadeOut('slow');
                this.location = this.router.url;
                if (!(event instanceof NavigationEnd)) {
                    return;
                }
                window.scrollTo(0, 0);
            });
    }
}
