import {Component, OnInit} from '@angular/core';
import {AccountService} from '../../../_services/account.service';
import {Roles} from '../../../_enums/roles';

@Component({
    selector: 'app-navbar-two',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {

    roles = Roles;

    constructor(
        private accountService: AccountService
    ) {
    }

    user: any;

    ngOnInit(): void {
        this.user = this.accountService.userValue;
    }

    onLogout() {
        this.accountService.logout();
    }
}
