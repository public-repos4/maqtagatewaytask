import { Component, OnInit } from '@angular/core';
import {LoaderService} from "../../../_services/loader.service";

@Component({
  selector: 'app-preloader',
  templateUrl: './preloader.component.html',
  styleUrls: ['./preloader.component.scss']
})
export class PreloaderComponent implements OnInit {

  constructor(public loadingService: LoaderService) { }

  ngOnInit(): void {

  }
}
