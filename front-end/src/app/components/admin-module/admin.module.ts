import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../../shared.module";
import {NavbarComponent} from "../layouts/navbar/navbar.component";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {CommonModule} from "@angular/common";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {DataTablesModule} from "angular-datatables";
import {NgSelectModule} from "@ng-select/ng-select";
import {UsersListComponent} from "./pages/users/users-list.component";
import {AddEditUserDialogComponent} from "./pages/users/dialogs/add-edit-user-dialog/add-edit-user-dialog.component";
import {DeleteUserConfirmationDialogComponent} from "./pages/users/dialogs/delete-user-confirmation-dialog/delete-user-confirmation-dialog.component";

const routes: Routes = [
    {
        path: 'users', component: UsersListComponent,
        children: [
            {path: 'add', component: AddEditUserDialogComponent},
            {path: 'edit/:id', component: AddEditUserDialogComponent},
            {path: 'delete/:id', component: DeleteUserConfirmationDialogComponent}
        ],
    },
];

@NgModule({
    declarations: [
        UsersListComponent,
        AddEditUserDialogComponent,
        DeleteUserConfirmationDialogComponent,

    ],
    imports: [
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (http: HttpClient) => new TranslateHttpLoader(http, ' ./assets/i18n/', '.json'),
                deps: [HttpClient]
            }
        }),

        CommonModule,
        FormsModule,
        NgbModule,
        DataTablesModule,
        NgSelectModule,
        SharedModule,

    ],
    exports: [
        NavbarComponent,
    ],
    providers: [],
})
export class AdminModule {

}
