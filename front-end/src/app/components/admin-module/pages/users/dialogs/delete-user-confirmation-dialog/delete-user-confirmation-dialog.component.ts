﻿import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AlertService} from "../../../../../../_services/alert.service";
import {first} from "rxjs/operators";
import {UsersService} from "../../../../../../_services/users.service";

@Component({
    selector: 'delete-user-confirmation-dialog',
    templateUrl: './delete-user-confirmation-dialog.component.html',
})
export class DeleteUserConfirmationDialogComponent implements OnInit {

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private modalService: NgbModal,
        private alertService: AlertService,
        private usersService: UsersService,
    ) {
    }

    @ViewChild('deleteConfirmationModal', {static: true}) deleteConfirmationModal: TemplateRef<any>;

    id: number;
    loading = false;
    submitted = false;

    public ngOnInit(): void {
        this.id = this.route.snapshot.params['id'];

        this.modalService.open(this.deleteConfirmationModal, {
            size: 'sm',
            // modalDialogClass: 'modal-full-height-middle'
        }).result.then((result) => {
            this.router.navigateByUrl('/admin/users');
        }, (reason) => {
            this.router.navigateByUrl('/admin/users');
            // console.log('reason')
        });
    }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        this.loading = true;
            this.deleteUser();
    }

    private deleteUser() {
        this.usersService.delete(this.id)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('SucceededDelete', {keepAfterRouteChange: true, autoClose: true});
                    this.modalService.dismissAll();
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }

}
