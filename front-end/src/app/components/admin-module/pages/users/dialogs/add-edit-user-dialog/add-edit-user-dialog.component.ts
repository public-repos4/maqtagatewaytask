import {Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {first, takeUntil} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {UsersService} from "../../../../../../_services/users.service";
import {AlertService} from "../../../../../../_services/alert.service";
import {SelectOption} from "../../../../../../_models/select-option";
import {RolesService} from "../../../../../../_services/roles.service";

@Component({
    selector: 'add-edit-user-dialog',
    templateUrl: './add-edit-user-dialog.component.html',
})
export class AddEditUserDialogComponent implements OnInit, OnDestroy {

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private modalService: NgbModal,
        private alertService: AlertService,
        private usersService: UsersService,
        private rolesService: RolesService,
    ) {
    }

    @ViewChild('addEditModal', {static: true}) addEditModal: TemplateRef<any>;
    destroy = new Subject<any>();

    form: FormGroup;
    id: number;
    isAddMode: boolean;
    loading = false;
    submitted = false;

    rolesPickList: Observable<SelectOption[]>;
    roleId: number;

    public ngOnInit(): void {
        this.id = this.route.snapshot.params['id'];
        this.isAddMode = !this.id;

        this.rolesPickList = this.rolesService.getRolesSelect();
        this.rolesPickList.subscribe((items) => {
            this.roleId = items[0].id;
        });

        this.form = this.formBuilder.group({
            id: [],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            userName: ['', Validators.required],
            roleId: ['', Validators.required],
            email: ['', Validators.required],
            age: ['', [Validators.required, Validators.min(18), Validators.max(100)]],
            phoneNumber: ['', Validators.required],
            mobileNumber: ['', Validators.required],
            address: ['', Validators.required],
            password: [''],
        });

        if (!this.isAddMode) {
            this.usersService.getById(this.id)
                .pipe(first())
                .subscribe(admin => this.form.patchValue(admin));
        }

        this.modalService.open(this.addEditModal, {
            size: '',
            // modalDialogClass: 'modal-full-height-middle'
        }).result.then((result) => {
            this.router.navigateByUrl('/admin/users');
        }, (reason) => {
            this.router.navigateByUrl('/admin/users');
            // console.log('reason')
        });
    }

    ngOnDestroy() {
        this.destroy.next();
    }

    // convenience getter for easy access to form fields
    get userForm() {
        return this.form.controls;
    }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        if (this.isAddMode) {
            this.addAdmin();
        } else {
            this.editAdmin();
        }
    }

    private addAdmin() {
        this.usersService.add(this.form.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('SucceededAdd', {keepAfterRouteChange: true, autoClose: true});
                    this.modalService.dismissAll();
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }

    private editAdmin() {
        this.usersService.edit(this.id, this.form.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('SucceededEdit', {keepAfterRouteChange: true, autoClose: true});
                    this.modalService.dismissAll();
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }
}
