import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild, Renderer2} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {environment} from "../../../../../environments/environment";
import {DataTableDirective} from 'angular-datatables';
import {AccountService} from "../../../../_services/account.service";

@Component({
    selector: 'users-list',
    templateUrl: './users-list.component.html',
})

export class UsersListComponent implements OnInit, AfterViewInit, OnDestroy {
    user: any;

    @ViewChild(DataTableDirective, {static: false}) dtElement: DataTableDirective;
    dtOptions: DataTables.Settings;
    dtTrigger: Subject<any> = new Subject<any>();

    constructor(
        private renderer: Renderer2,
        private router: Router,
        private accountService: AccountService
    ) {
    }

    ngOnInit(): void {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd && this.router.url.endsWith('/users')) {
                this.rerenderDatatable();
            }
        });

        this.user = this.accountService.userValue;

        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 10,
            serverSide: true,
            processing: true,

            ajax: {
                headers: {
                    'Authorization': `Bearer ${this.user.token}`,
                    'Accept-Language': localStorage.getItem('lang')
                },
                type: 'Post',
                url: `${environment.apiUrl}/users/getDatatable`,
            },

            rowCallback: (row: Node, data: any[] | Object, index: number) => {
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                // Note: In newer jQuery v3 versions, `unbind` and `bind` are
                // deprecated in favor of `off` and `on`
                $(row).find('[data-title="openEditUserForm"]').off('click');
                $(row).find('[data-title="openEditUserForm"]').on('click', () => {
                    this.router.navigateByUrl(`/admin/users/edit/${data['id']}`);
                });

                $(row).find('[data-title="openDeleteUserConfirm"]').off('click');
                $(row).find('[data-title="openDeleteUserConfirm"]').on('click', () => {
                    this.router.navigateByUrl(`/admin/users/delete/${data['id']}`);
                });
                return row;
            },

            columnDefs: [
                {
                    targets: '_all',
                    createdCell: function (td, cellData, rowData, row, col) {
                        let usersTblCellsDataTitles = ['id', 'firstName', 'lastName', 'userName', 'role', 'email', 'mobileNumber', 'phoneNumber', 'age', 'address', 'isActive'];
                        $(td).attr('data-title', usersTblCellsDataTitles[col]);
                    },
                    defaultContent: ""
                },
                {
                    targets: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                    orderable: false,
                },
            ],

            columns: [{
                data: 'id',
                name: 'id'
            }, {
                data: 'firstName',
                name: 'firstName'
            }, {
                data: 'lastName',
                name: 'lastName'
            }, {
                data: 'userName',
                name: 'userName'
            }, {
                data: 'role.name',
                name: 'role'
            }, {
                data: 'email',
                name: 'email'
            }, {
                data: 'mobileNumber',
                name: 'mobileNumber'
            }, {
                data: 'phoneNumber',
                name: 'phoneNumber'
            }, {
                data: 'age',
                name: 'age'
            }, {
                data: 'address',
                name: 'address'
            }, {
                data: null,
                name: 'actions',
                render: function (data: any, type: any, full: any) {
                    let actions = '';
                    actions += `<i class="cursor-pointer icon-compose mr-2" data-title="openEditUserForm"></i>`;
                    actions += `<i class="cursor-pointer icon-bin mr-2" data-title="openDeleteUserConfirm"></i>`;
                    return actions;
                }
            }]
        };
    }

    ngAfterViewInit(): void {
        this.dtTrigger.next();
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    rerenderDatatable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();

            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }
}
