import {Component, OnInit} from '@angular/core';
import {AccountService} from '../../../../_services/account.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
})

export class HomeComponent implements OnInit {
    user: any;

    constructor(
        private accountService: AccountService
    ) {
    }


    ngOnInit(): void {
        this.user = this.accountService.userValue;
    }
}
