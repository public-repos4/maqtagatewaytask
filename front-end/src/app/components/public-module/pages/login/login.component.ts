import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AlertService} from 'src/app/_services/alert.service';
import {AccountService} from 'src/app/_services/account.service';
import {Roles} from '../../../../_enums/roles';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService
    ) {
    }

    roles = Roles;

    loginFormGroup: FormGroup;
    loginLoading = false;
    loginSubmitted = false;

    ngOnInit(): void {
        this.loginFormGroup = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    // convenience getter for easy access to form fields
    get loginForm() {
        return this.loginFormGroup.controls;
    }

    onLoginSubmit() {
        this.loginSubmitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.loginFormGroup.invalid) {
            return;
        }

        this.loginLoading = true;

        this.accountService.login(this.loginForm.username.value, this.loginForm.password.value)
            .pipe(first())
            .subscribe({
                next: (user) => {
                    let returnUrl = '/';
                    if (this.route.snapshot.queryParams.returnUrl) {
                        returnUrl = this.route.snapshot.queryParams.returnUrl;
                    }

                    this.router.navigateByUrl(returnUrl);
                },
                error: error => {
                    this.alertService.error(error);
                    this.loginLoading = false;
                }
            });
    }
}
