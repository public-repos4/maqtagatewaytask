import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../../shared.module";
import {NavbarComponent} from "../layouts/navbar/navbar.component";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {NgxGalleryModule} from "@kolkov/ngx-gallery";

import {HomeComponent} from "./pages/home/home.component";
import {LoginComponent} from "./pages/login/login.component";
import {PageNotFoundComponent} from "./pages/page-not-found/page-not-found.component";
import {PaginationModule} from "ngx-bootstrap/pagination";

const routes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'login', component: LoginComponent},
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    declarations: [
        LoginComponent,
        HomeComponent,
        PageNotFoundComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (http: HttpClient) => new TranslateHttpLoader(http, ' ./assets/i18n/', '.json'),
                deps: [HttpClient]
            }
        }),

        FormsModule,
        NgxGalleryModule,
        PaginationModule.forRoot(),

        SharedModule,
    ],
    exports: [
        NavbarComponent,
    ],
    providers: [],
})
export class PublicModule {

}
