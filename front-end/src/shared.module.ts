import {ModuleWithProviders, NgModule} from '@angular/core';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {NavbarComponent} from "./app/components/layouts/navbar/navbar.component";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";

@NgModule({
    declarations: [
        // AlertComponent,
        // PreloaderComponent,
        NavbarComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
    ],
    exports: [
        CommonModule,
        TranslateModule,

        // AlertComponent,
        // PreloaderComponent,
        NavbarComponent,
    ],
})

export class SharedModule {

}
